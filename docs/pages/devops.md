# DevOps / DevSecOps

## Planifier

### JiraSoftware

### Confluence

### Slack


## Build

### Kubernetes

https://kubernetes.io/fr/

Solution professionnelle d'orchestration de conteneurs

### Docker

### Puppet

https://puppet.com/

### Terraform

### Ansible

### Chef Progress

https://www.chef.io/

### GitLab

### GitHub

### Bitbucket

## Intégration continue et livraison 

### Jenkins

### SonarSource

### Bitbucket



## Autres

### Prometheus

https://prometheus.io/

Prometheus est un logiciel libre de surveillance informatique et générateur d'alertes. Il enregistre des métriques en temps réel dans une base de données de séries temporelles en se basant sur le contenu de point d'entrée exposé à l'aide du protocole HTTP.

### Vault

https://www.vaultproject.io/

Sécurisez, stockez et contrôlez étroitement l'accès aux jetons, mots de passe, certificats, clés de chiffrement pour protéger les secrets et autres données sensibles à l'aide d'une interface utilisateur, d'une CLI ou d'une API HTTP.





